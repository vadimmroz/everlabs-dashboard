import { configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import todoList from "./slices/todoListSlice.ts";
import sortedTaskListSlice from "./slices/sortedTaskListSlice.ts";
import modalAddTaskSlice from "./slices/modalAddTaskSlice.ts";
import userDataSlice from "./slices/userDataSlice.ts";
import taskSlice from "./slices/taskSlice.ts";
import modalAddProjectSlice from "./slices/modalAddProjectSlice.ts";
import addTaskSlice from "./slices/addTaskSlice.ts";
import addProjectSlice from "./slices/addProjectSlice.ts";
import projectListSlice from "./slices/projectListSlice.ts";
import projectSlice from "./slices/projectSlice.ts";
import profileSlice from "@store/slices/profileSlice.ts";

const store = configureStore({
  reducer: {
    todoList: todoList,
    projectList: projectListSlice,
    sortedTaskList: sortedTaskListSlice,
    modalAddTask: modalAddTaskSlice,
    userData: userDataSlice,
    task: taskSlice,
    modalAddProject: modalAddProjectSlice,
    addTask: addTaskSlice,
    addProject: addProjectSlice,
    project: projectSlice,
    profile: profileSlice,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }),
});

export default store;

export const useAppDispatch: () => typeof store.dispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<
  ReturnType<typeof store.getState>
> = useSelector;

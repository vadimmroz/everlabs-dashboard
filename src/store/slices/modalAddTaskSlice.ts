import { createSlice } from "@reduxjs/toolkit";

const modalAddTaskSlice = createSlice({
  name: "modalAddTask",
  initialState: {
    modalAddTask: false,
  },
  reducers: {
    handleOpen(state) {
      state.modalAddTask = true;
    },
    handleClose(state) {
      state.modalAddTask = false;
    },
  },
});

export const { handleOpen, handleClose } = modalAddTaskSlice.actions;

export default modalAddTaskSlice.reducer;

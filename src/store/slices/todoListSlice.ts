import { createSlice } from "@reduxjs/toolkit";
import { ITask } from "@/types/ITask.ts";

type Action = { type: string; payload: ITask };
type SetAction = { type: string; payload: ITask[] };

interface IInitialState {
  todoList: Array<ITask>;
}

const initialState: IInitialState = {
  todoList: [],
};

const todoListSlice = createSlice({
  name: "todoList",
  initialState,
  reducers: {
    addTodo(state, action: Action) {
      state.todoList.push(action.payload);
    },
    deleteTodo(state, action: Action) {
      state.todoList = state.todoList.filter(
        (e: ITask) => e.id !== action.payload.id,
      );
    },
    updateTodo(state, action: Action) {
      state.todoList = state.todoList.map((e: ITask) => {
        if (e.id === action.payload.id) {
          return action.payload;
        } else {
          return e;
        }
      });
    },
    setTodo(state, action: SetAction) {
      state.todoList = action.payload;
    },
  },
});

export const { addTodo, setTodo, deleteTodo, updateTodo } =
  todoListSlice.actions;

export default todoListSlice.reducer;

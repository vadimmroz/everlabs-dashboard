import { createSlice } from "@reduxjs/toolkit";

const modalAddProjectSlice = createSlice({
  name: "modalAddProject",
  initialState: {
    modalAddProject: false,
  },
  reducers: {
    handleOpenProjectModal(state) {
      state.modalAddProject = true;
    },
    handleCloseProjectModal(state) {
      state.modalAddProject = false;
    },
  },
});

export const { handleOpenProjectModal, handleCloseProjectModal } =
  modalAddProjectSlice.actions;

export default modalAddProjectSlice.reducer;

import { createSlice } from "@reduxjs/toolkit";
import { IProject } from "@/types/IProject.ts";
import moment from "moment";

type Action = { type: string; payload: IProject };

interface IInitialState {
  addProject: IProject;
}

const initialState: IInitialState = {
  addProject: {
    id: Math.floor(Math.random() * 1000),
    name: "",
    description: "",
    members: [{ email: localStorage.getItem("login") || "", role: "owner" }],
    color: "#ffffff",
    tasks: [],
    reports: [],
    created_at: moment(),
    author: localStorage.getItem("login") || "",
  },
};

const addProjectSlice = createSlice({
  name: "addTask",
  initialState,
  reducers: {
    setAddProject(state, action: Action) {
      state.addProject = action.payload;
    },
  },
});

export const { setAddProject } = addProjectSlice.actions;

export default addProjectSlice.reducer;

import { createSlice } from "@reduxjs/toolkit";
import { ITask } from "@/types/ITask.ts";
import moment from "moment";

type Action = { type: string; payload: ITask };

interface IInitialState {
  addTask: ITask;
}

const initialState: IInitialState = {
  addTask: {
    created_at: moment(),
    author: localStorage.getItem("login") || "",
    members: {
      guest: [],
      moder: [],
      owner: [localStorage.getItem("login") || ""],
    },
    deadline: moment(),
    comments: [],
    color: "#ffffff",
    id: Math.floor(Math.random() * 1000),
    name: "",
    project: "",
    priority: "",
    status: "",
    description: "",
    projectColor: "",
    estimate: 0,
  },
};

const addTaskSlice = createSlice({
  name: "addTask",
  initialState,
  reducers: {
    setAddTask(state, action: Action) {
      state.addTask = action.payload;
    },
  },
});

export const { setAddTask } = addTaskSlice.actions;

export default addTaskSlice.reducer;

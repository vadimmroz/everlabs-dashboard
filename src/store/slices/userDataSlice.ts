import { createSlice } from "@reduxjs/toolkit";
import { IProfile } from "@/types/IProfile.ts";

type Action = { type: string; payload: IProfile };

interface IInitialState {
  userData: IProfile | null;
}

const initialState: IInitialState = {
  userData: null,
};

const userDataSlice = createSlice({
  name: "userData",
  initialState,
  reducers: {
    setUserData(state, action: Action) {
      state.userData = action.payload;
    },
  },
});

export const { setUserData } = userDataSlice.actions;
export default userDataSlice.reducer;

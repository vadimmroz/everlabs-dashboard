import { createSlice } from "@reduxjs/toolkit";
import { ITask } from "@/types/ITask.ts";

type Action = { type: string; payload: ITask };

interface IInitialState {
  task: ITask | null;
}

const initialState: IInitialState = {
  task: null,
};

const taskSlice = createSlice({
  name: "task",
  initialState,
  reducers: {
    setTask(state, action: Action) {
      state.task = action.payload;
    },
  },
});

export const { setTask } = taskSlice.actions;

export default taskSlice.reducer;

import { createSlice } from "@reduxjs/toolkit";
import { IProject } from "@/types/IProject.ts";

type Action = { type: string; payload: IProject };

interface IInitialState {
  project: IProject | null;
}

const initialState: IInitialState = {
  project: null,
};
const projectSlice = createSlice({
  name: "project",
  initialState,
  reducers: {
    setProject(state, action: Action) {
      state.project = action.payload;
    },
  },
});

export const { setProject } = projectSlice.actions;

export default projectSlice.reducer;

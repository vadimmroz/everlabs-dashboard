import { createSlice } from "@reduxjs/toolkit";
import { IProject } from "@/types/IProject.ts";

type Action = { type: string; payload: IProject[] | undefined };

interface IInitialState {
  projectList: Array<IProject>;
}

const initialState: IInitialState = {
  projectList: [],
};
const projectListSlice = createSlice({
  name: "projectList",
  initialState,
  reducers: {
    setProjectList(state, action: Action) {
      if (action.payload) {
        state.projectList = action.payload;
      }
    },
  },
});

export const { setProjectList } = projectListSlice.actions;

export default projectListSlice.reducer;

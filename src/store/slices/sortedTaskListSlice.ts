import { createSlice } from "@reduxjs/toolkit";
import { ITask } from "@/types/ITask.ts";

type SetAction = { type: string; payload: ITask[] };

interface IInitialState {
  sortedTaskList: Array<ITask>;
}

const initialState: IInitialState = {
  sortedTaskList: [],
};

const sortedTaskListSlice = createSlice({
  name: "sortedTaskList",
  initialState,
  reducers: {
    setSortedTaskList(state, action: SetAction) {
      state.sortedTaskList = action.payload;
    },
  },
});

export const { setSortedTaskList } = sortedTaskListSlice.actions;
export default sortedTaskListSlice.reducer;

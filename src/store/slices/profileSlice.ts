import { createSlice } from "@reduxjs/toolkit";
import { IProfile } from "@/types/IProfile.ts";

type Action = { type: string; payload: IProfile | undefined };

interface IInitialState {
  profile: IProfile | undefined;
}

const initialState: IInitialState = {
  profile: undefined,
};

const profileSlice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    setProfile(state, action: Action) {
      state.profile = action.payload;
    },
  },
});

export const { setProfile } = profileSlice.actions;

export default profileSlice.reducer;

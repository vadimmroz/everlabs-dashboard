export type notificationType = [];
export interface IProfile {
  id: number;
  created_at: number;
  name: string;
  email: string;
  img: string | null;
  ownerProject: string[];
  memberProject: string[];
  guestProject: string[];
  description: string;
  surname: string;
  notification: notificationType;
  tasksOnWork: string[];
}

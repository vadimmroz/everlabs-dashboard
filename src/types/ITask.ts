import { Moment } from "moment/moment";

type Comment = {
  date: number;
  name: string;
  text: string;
};

export interface ITask {
  created_at: Moment;
  author: string | null;
  members: {
    guest: Array<string>;
    moder: Array<string>;
    owner: Array<string>;
  };
  deadline: Moment;
  comments: Array<Comment>;
  color: string;
  id: number;
  name: string;
  project: string;
  priority: string;
  status: string;
  description: string;
  projectColor: string;
  estimate: number;
}

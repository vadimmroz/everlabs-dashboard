import { Moment } from "moment";

export type Member = {
  email: string;
  role: string;
};
type Reports = [];

export interface IProject {
  id: number;
  name: string;
  description: string;
  members: Member[];
  color: string;
  tasks: string[];
  reports: Reports;
  created_at: Moment;
  author: string;
}

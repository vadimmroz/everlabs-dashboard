export const ROUTES = {
  AUTH: {
    name: "Authentication page",
    path: "/auth",
    key: "auth",
  },
  HOME: {
    name: "Home page",
    path: "/home",
    key: "home",
    children: {
      TASKS: {
        name: "Tasks page",
        path: "tasks",
        key: "tasks",
        children: {
          ID: {
            name: "simple taskPage page",
            path: "/task/:id",
            key: "task",
          },
        },
      },
      PROJECTS: {
        name: "ProjectsPage page",
        path: "projects",
        key: "projects",
        children: {
          ID: {
            name: "simple project page",
            path: "/project/:id",
            key: "project",
          },
        },
      },
      REPORTS: {
        name: "Reports page",
        path: "reports",
        key: "reports",
        children: {
          ID: {
            name: "simple report page",
            path: "/report/:id",
            key: "report",
          },
        },
      },
    },
  },
  PROFILE: {
    name: "Profile page",
    path: "/profile/:id",
    key: "profile",
  },
  DEFAULT: {
    name: "Default Routes path",
    path: "/*",
    key: "default",
    navigateTo: {
      name: "Navigate to Default page after default routes",
      path: "/home/tasks",
      key: "navigate",
    },
  },
};

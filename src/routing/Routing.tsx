import { Navigate, Route, Routes } from "react-router-dom";
import { ROUTES } from "./routes.ts";
import { lazy, Suspense } from "react";

const Auth = lazy(() => import("@/pages/auth"));
const TasksPage = lazy(() => import("@/pages/tasksPage"));
const ProjectsPage = lazy(() => import("@/pages/projectsPage"));
const Reports = lazy(() => import("@/pages/reports"));
const MyTodoList = lazy(() => import("@/components/myTodoList"));
const Project = lazy(() => import("@/pages/project"));
const Task = lazy(() => import("@/pages/task"));
const Profile = lazy(() => import("@/pages/profile"));
const Routing = () => {
  return (
    <Routes>
      <Route
        path="/*"
        element={
          <Navigate
            to={ROUTES.DEFAULT.navigateTo.path}
            key={ROUTES.DEFAULT.navigateTo.key}
          />
        }
      />
      <Route
        path={ROUTES.AUTH.path}
        key={ROUTES.AUTH.key}
        element={
          <Suspense>
            <Auth />
          </Suspense>
        }
      />
      <Route
        path={ROUTES.HOME.children.TASKS.children.ID.path}
        key={ROUTES.HOME.children.TASKS.children.ID.key}
        element={
          <Suspense>
            <Task />
          </Suspense>
        }
      />
      <Route
        path={ROUTES.HOME.children.PROJECTS.children.ID.path}
        key={ROUTES.HOME.children.PROJECTS.children.ID.key}
        element={
          <Suspense>
            <Project />
          </Suspense>
        }
      />
      <Route
        path={ROUTES.HOME.children.REPORTS.children.ID.path}
        key={ROUTES.HOME.children.REPORTS.children.ID.key}
        element={<h1>report</h1>}
      />
      <Route
        path={ROUTES.HOME.path}
        key={ROUTES.HOME.key}
        element={
          <Suspense>
            <MyTodoList />
          </Suspense>
        }
      >
        <Route
          path={ROUTES.HOME.children.TASKS.path}
          key={ROUTES.HOME.children.TASKS.key}
          element={
            <Suspense>
              <TasksPage />
            </Suspense>
          }
        />
        <Route
          path={ROUTES.HOME.children.PROJECTS.path}
          key={ROUTES.HOME.children.PROJECTS.key}
          element={
            <Suspense>
              <ProjectsPage />
            </Suspense>
          }
        />
        <Route
          path={ROUTES.HOME.children.REPORTS.path}
          key={ROUTES.HOME.children.REPORTS.key}
          element={
            <Suspense>
              <Reports />
            </Suspense>
          }
        />
      </Route>
      <Route
        path={ROUTES.PROFILE.path}
        key={ROUTES.PROFILE.key}
        element={
          <Suspense>
            <Profile />
          </Suspense>
        }
      />
    </Routes>
  );
};

export default Routing;

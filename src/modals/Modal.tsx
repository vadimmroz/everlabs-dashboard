import { lazy, Suspense } from "react";
import { useAppSelector } from "@store/store.ts";

const ModalPortal = lazy(() => import("@/modals/ModalPortal.tsx"));
const ModalAddTask = lazy(() => import("@components/modalAddTask"));
const ModalCreateProject = lazy(
  () => import("@components/modalCreateProject/ModalCreateProject.tsx"),
);

const Modal = () => {
  const modalAddTask = useAppSelector(
    (state) => state.modalAddTask.modalAddTask,
  );
  const modalAddProject = useAppSelector(
    (state) => state.modalAddProject.modalAddProject,
  );
  return (
    <ModalPortal>
      {modalAddTask && (
        <Suspense>
          <ModalAddTask />
        </Suspense>
      )}
      {modalAddProject && (
        <Suspense>
          <ModalCreateProject />
        </Suspense>
      )}
    </ModalPortal>
  );
};

export default Modal;

import { useEffect, useMemo, JSX } from "react";
import { createPortal } from "react-dom";

const modalRootElement = document.querySelector("#modal");

const ModalPortal = (props: {
  children: string | JSX.Element | JSX.Element[];
}) => {
  const element = useMemo(() => document.createElement("div"), []);
  useEffect(() => {
    modalRootElement?.appendChild(element);
    return () => {
      modalRootElement?.removeChild(element);
    };
  }, [element]);
  return createPortal(props.children, element);
};

export default ModalPortal;

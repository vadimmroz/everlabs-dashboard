import { supabase } from "./config.ts";
import { Params } from "react-router-dom";
import { ITask } from "@/types/ITask.ts";
import { IProject } from "@/types/IProject.ts";

export const getAllProjects = async () => {
  return supabase
    .from("EverlabsDashboardsProjects")
    .select("*")
    .like("author", "vadimmroz@gmail.com");
};

export const getProjectById = async (params: Readonly<Params<string>>) => {
  return supabase
    .from("EverlabsDashboardsProjects")
    .select("*")
    .like("author", "vadimmroz@gmail.com")
    .like("name", params.id || "")
    .order("id", { ascending: true });
};
export const getProjectByName = async (name: string | undefined) => {
  return supabase
    .from("EverlabsDashboardsProjects")
    .select("*")
    .like("author", "vadimmroz@gmail.com")
    .like("name", name || "")
    .order("id", { ascending: true });
};

export const getAllTaskByProjectName = async (
  params: Readonly<Params<string>>,
) => {
  return supabase
    .from("EverlabsDashboardTasks")
    .select("*")
    .like("project", params.id || "")
    .order("created_at", { ascending: false });
};

export const getAllTaskByUserName = async (user: string | null) => {
  return supabase
    .from("EverlabsDashboardTasks")
    .select("*")
    .like("author", user || "")
    .order("created_at", { ascending: false });
};

export const updateTask = async (task: ITask) => {
  return supabase.from("EverlabsDashboardTasks").upsert(task).eq("id", task.id);
};

export const signUpNewUser = async (
  email: string,
  password: string,
  phone: string,
) => {
  return await supabase.auth.signUp({
    email: email,
    password: password,
    phone: phone,
  });
};

export const signInWithEmail = async (
  email: string | null,
  password: string | null,
) => {
  return await supabase.auth.signInWithPassword({
    email: email || "",
    password: password || "",
  });
};

export const getUserProfileData = async (email: string | undefined) => {
  return supabase
    .from("usersProfiles")
    .select("*")
    .eq("email", email || "");
};

export const postTask = async (task: ITask, project: IProject) => {
  supabase
    .from("EverlabsDashboardsProjects")
    .update({ tasks: [...project.tasks, task.name] })
    .eq("id", project.id);
  return supabase.from("EverlabsDashboardTasks").insert(task);
};

export const getTaskById = async (params: Readonly<Params<string>>) => {
  return supabase
    .from("EverlabsDashboardTasks")
    .select("*")
    .eq("id", params.id || "");
};

export const getUsersProfilesByEmail = async (
  members: string[] | undefined,
) => {
  return supabase
    .from("usersProfiles")
    .select("*")
    .in("email", members || []);
};

export const deleteTask = async (task: ITask) => {
  return supabase.from("EverlabsDashboardTasks").delete().eq("id", task.id);
};

export const AddProject = async (project: IProject) => {
  project.members.forEach((member) => {
    supabase
      .from("usersProfiles")
      .select("*")
      .eq("email", member.email)
      .then((res) => {
        if (res?.data) {
          supabase
            .from("usersProfiles")
            .update({
              [member.role + "Project"]: [
                ...res.data[0][member.role + "Project"],
                project.name,
              ],
            })
            .eq("email", member.email);
        }
      });
  });
  return supabase.from("EverlabsDashboardsProjects").insert(project);
};

export const saveProject = (project: IProject) => {
  return supabase
    .from("EverlabsDashboardsProjects")
    .upsert(project)
    .eq("id", project.id);
};

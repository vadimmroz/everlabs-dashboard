import { createClient } from "@supabase/supabase-js";

export const supabase = createClient(
  import.meta.env.VITE_URL.toString(),
  import.meta.env.VITE_API.toString(),
);

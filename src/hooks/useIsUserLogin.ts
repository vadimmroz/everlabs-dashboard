import { useState, useEffect } from "react";
import { getUserProfileData, signInWithEmail } from "@api/api.ts";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch } from "@store/store.ts";
import { setUserData } from "@store/slices/userDataSlice.ts";
import { PostgrestSingleResponse } from "@supabase/supabase-js";
import { IProfile } from "@/types/IProfile.ts";

const useIsUserLogin = () => {
  const [isUserLogin, setIsUserLogin] = useState(false);
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const dispatch = useAppDispatch();

  useEffect(() => {
    const checkUserLogin = async () => {
      const login = localStorage.getItem("login");
      const password = localStorage.getItem("password");
      if (login && password) {
        try {
          const res = await signInWithEmail(login, password);
          if (res.data) {
            if (pathname === "/auth") {
              navigate("/home/tasksPage");
            }
            getUserProfileData(login).then(
              (res: PostgrestSingleResponse<IProfile[]>) => {
                if (res.data) {
                  dispatch(setUserData(res.data[0]));
                }
              },
            );

            setIsUserLogin(true);
          } else {
            navigate("/auth");
          }
        } catch (error) {
          console.error("Помилка під час автентифікації:", error);
          navigate("/auth");
        }
      } else {
        navigate("/auth");
      }
    };
    checkUserLogin();
  }, [navigate, dispatch, pathname]);

  return isUserLogin;
};

export default useIsUserLogin;

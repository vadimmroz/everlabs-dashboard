import { lazy, Suspense } from "react";
import useIsUserLogin from "./hooks/useIsUserLogin.ts";
import Routing from "./routing";

const Header = lazy(() => import("./components/header"));
const Modal = lazy(() => import("./modals/Modal.tsx"));

function App() {
  const isUserLogin = useIsUserLogin();

  return (
    <>
      {isUserLogin && (
        <Suspense>
          <Header />
          <Modal />
        </Suspense>
      )}
      <Routing />
    </>
  );
}

export default App;

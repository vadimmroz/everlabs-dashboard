import notifycation from "@assets/headerImg/notification.png";
import { Box, Button, Typography } from "@mui/material";
import NavBar from "./components/navBar";
import { Link, useLocation } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { handleOpen } from "@store/slices/modalAddTaskSlice.ts";
import { handleOpenProjectModal } from "@store/slices/modalAddProjectSlice.ts";

const Header = () => {
  const dispatch = useAppDispatch();
  const handleOpenAddTaskModal = () => {
    dispatch(handleOpen());
  };
  const handleOpenAddProject = () => {
    dispatch(handleOpenProjectModal());
  };
  const params = useLocation();
  const userData = useAppSelector((state) => state.userData.userData);

  return (
    <Box
      py={3}
      px={2}
      flexDirection="row"
      display="flex"
      gap={2}
      position="relative"
    >
      <Box
        flexDirection="row"
        display="flex"
        gap={2}
        position="relative"
        sx={{ maxWidth: 350, width: "100%" }}
      >
        <Link
          to={"/profile/" + userData?.email}
          style={{
            overflow: "hidden",
            borderRadius: "50%",
            width: 50,
            height: 50,
          }}
        >
          <img
            src={userData?.img || ""}
            alt="avatar"
            style={{ width: 50, height: 50, objectFit: "cover" }}
          />
        </Link>
        <Box>
          <Typography fontWeight="bold">{userData?.name}</Typography>
          <Typography color="#787486">{userData?.email}</Typography>
        </Box>
        <Box ml="auto" position="absolute" right={0}>
          <img src={notifycation} alt="notifycation" />
          <div
            style={{
              position: "absolute",
              right: 0,
              top: 0,
              borderRadius: "50%",
              background: "#D8727D",
              width: 8,
              height: 8,
            }}
          />
        </Box>
      </Box>
      <NavBar />
      {params.pathname === "/home/projects" ? (
        <Button
          color="secondary"
          variant="contained"
          sx={{ width: 180, height: 50, marginLeft: "auto" }}
          onClick={handleOpenAddProject}
        >
          + Project
        </Button>
      ) : (
        <Button
          color="secondary"
          variant="contained"
          sx={{ width: 180, height: 50, marginLeft: "auto" }}
          onClick={handleOpenAddTaskModal}
        >
          + Add Task
        </Button>
      )}
    </Box>
  );
};

export default Header;

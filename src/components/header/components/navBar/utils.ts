import projects from "@assets/navLinkIcons/projects.svg";
import tasks from "@assets/navLinkIcons/tasks.svg";
import reports from "@assets/navLinkIcons/reports.svg";

export const navBarLinks = [
  {
    img: tasks,
    link: "/home/tasks",
    name: "Tasks",
  },
  {
    img: projects,
    link: "/home/projects",
    name: "Projects",
  },
  {
    img: reports,
    link: "/home/reports",
    name: "Reports",
  },
];

import { Box } from "@mui/material";
import NavBarLink from "@components/navBarLink";
import { navBarLinks } from "./utils.ts";

const NavBar = () => {
  return (
    <Box display="flex" flexDirection="row" gap={2} mx={5}>
      {navBarLinks.map((e, i) => (
        <NavBarLink img={e.img} link={e.link} name={e.name} key={i} />
      ))}
    </Box>
  );
};

export default NavBar;

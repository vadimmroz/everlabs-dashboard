import { Box, Grid, Typography } from "@mui/material";
import { useAppSelector } from "@store/store.ts";
import TaskCard from "@components/taskCard";
import { Droppable } from "react-beautiful-dnd";

const TaskList = ({ name, color }: { name: string; color: string }) => {
  const sortedTaskList = useAppSelector(
    (state) => state.sortedTaskList.sortedTaskList,
  );

  return (
    <Grid item xs={1}>
      <Box display="flex" gap={2}>
        <div
          style={{
            borderRadius: "50%",
            width: 10,
            height: 10,
            background: color,
            margin: "auto 0",
          }}
        />
        <Typography color="#0D062D" fontWeight="bold">
          {name}
        </Typography>
      </Box>
      <Box
        sx={{
          background: color,
          width: "80%",
          height: 3,
          borderRadius: 2,
          marginTop: 2,
        }}
      />
      <Droppable droppableId={name}>
        {(provided) => (
          <Box
            {...provided.droppableProps}
            ref={provided.innerRef}
            display="flex"
            flexDirection="column"
            sx={{ overflowY: "scroll", overflowX: "hidden" }}
            height="80vh"
          >
            {sortedTaskList &&
              sortedTaskList
                .filter((e) => e.status === name)
                .map((e) => <TaskCard key={e.id} element={e} />)}
          </Box>
        )}
      </Droppable>
    </Grid>
  );
};

export default TaskList;

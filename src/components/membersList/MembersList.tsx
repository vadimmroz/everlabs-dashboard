import { IProfile } from "@/types/IProfile.ts";
import { Box } from "@mui/material";
import styles from "./styles.ts";

const MembersList = ({ members }: { members: IProfile[] }) => {
  return (
    <Box sx={styles.list}>
      {members?.map((e) => {
        return (
          <Box key={e.id} sx={styles.user}>
            <img
              src={e.img || ""}
              alt={e.name}
              style={{
                borderRadius: "50%",
                overflow: "hidden",
                width: 40,
                height: 40,
                objectFit: "cover",
              }}
            />
          </Box>
        );
      })}
    </Box>
  );
};

export default MembersList;

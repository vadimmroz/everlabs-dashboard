import { Outlet } from "react-router-dom";
import { Grid, Box } from "@mui/material";
import { useAppDispatch } from "@store/store.ts";
import classes from "./myTodoList.module.css";
import { setTodo } from "@store/slices/todoListSlice.ts";
import { useEffect } from "react";
import { setSortedTaskList } from "@store/slices/sortedTaskListSlice.ts";
import { useQuery } from "react-query";
import { getAllTaskByUserName } from "@api/api.ts";
import MyTodoListHeader from "./components/MyTodoListHeader.tsx";
import MyTodoListBody from "./components/MyTodoListBody.tsx";
import styles from "./styles.ts";

const MyTodoList = () => {
  const dispatch = useAppDispatch();

  const tasks = useQuery("taskByUserName", () =>
    getAllTaskByUserName(localStorage.getItem("login")),
  );

  useEffect(() => {
    if (tasks?.data?.data) {
      dispatch(setSortedTaskList(tasks.data.data));
      dispatch(setTodo(tasks?.data?.data));
    }
  }, [tasks, dispatch]);

  return (
    <Box sx={styles.container}>
      <Grid container columnSpacing={2} columns={{ xs: 4 }} sx={styles.wrapper}>
        <Grid item xs={1} sx={styles.myTodoList}>
          <MyTodoListHeader />
          <div className={classes.line} />
          <MyTodoListBody />
        </Grid>
        <Outlet />
      </Grid>
    </Box>
  );
};

export default MyTodoList;

export default {
  container: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
  },
  wrapper: { paddingLeft: 5, paddingRight: 2, paddingTop: 1, rowGap: 2 },
  myTodoList: {
    width: 380,
    maxWidth: 380,
    maxHeight: "87vh",
    position: "sticky",
    overflow: "hidden",
    padding: "16px 0",
    top: 0,
  },
};

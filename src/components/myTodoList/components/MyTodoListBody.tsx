import classes from "@components/myTodoList/myTodoList.module.css";
import Todo from "./todo";
import { Box } from "@mui/material";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { ITask } from "@/types/ITask.ts";
import { updateTodo } from "@store/slices/todoListSlice.ts";

const MyTodoListBody = () => {
  const dispatch = useAppDispatch();
  const todoList = useAppSelector((state) => state.todoList.todoList);
  const handleTodo = (e: ITask) => {
    dispatch(updateTodo(e));
  };
  return (
    <Box display="flex" flexDirection="column" className={classes.todolist}>
      {todoList?.map((e, i) => {
        return (
          <Todo handleTodo={handleTodo} element={e} key={e.id} iterator={i} />
        );
      })}
    </Box>
  );
};

export default MyTodoListBody;

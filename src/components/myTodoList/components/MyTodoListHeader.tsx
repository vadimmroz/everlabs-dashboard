import { Box, Grid, Typography } from "@mui/material";
import moment from "moment";

const MyTodoListHeader = () => {
  return (
    <Grid item width={340} maxWidth={340} py={2} xs={0.9}>
      <Box display="flex" justifyContent="space-between" mx="auto" mb={2}>
        <Typography fontWeight={700}>My To Do’s List</Typography>
        <Typography color="#787486">
          Today {moment().format("DD/MM/yyyy")}
        </Typography>
      </Box>
    </Grid>
  );
};

export default MyTodoListHeader;

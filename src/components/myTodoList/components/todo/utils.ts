export const descriptionParser = (str: string) => {
  if (str.length > 100) {
    return str.slice(0, -(str.length - 100)).trim() + "...";
  } else {
    return str;
  }
};

import { Box, Button, Typography } from "@mui/material";
import moment from "moment/moment";
import classes from "@components/myTodoList/myTodoList.module.css";
import PriorityStatusBox from "@components/priorityStatusBox";
import { ITask } from "@/types/ITask.ts";
import { descriptionParser } from "./utils.ts";

const Todo = ({
  element,
  iterator,
  handleTodo,
}: {
  element: ITask;
  iterator: number;
  handleTodo: (e: ITask) => void;
}) => {
  const handleClick = () => {
    handleTodo({ ...element });
  };

  return (
    <div onClick={handleClick} style={{ cursor: "pointer" }}>
      <Box display="flex" gap={8} marginBottom={3} position="relative">
        <Box width={75}>
          <Typography color={"#000"} fontWeight={"bolder"}>
            {moment(element.created_at).format("hh:mm A")}
          </Typography>
        </Box>
        <div className={classes.sphere}>
          <Typography>{iterator}</Typography>
        </div>
        <Box display="flex" flexDirection="column" sx={{ flexGrow: 1 }}>
          <Button
            variant="contained"
            color="secondary"
            size="small"
            sx={{
              width: "50%",
              height: 20,
              marginLeft: "auto",
              textOverflow: "ellipsis",
              textWrap: "nowrap",
              fontSize: 10,
              background: element.projectColor,
            }}
          >
            {element.project}
          </Button>
          <Typography fontWeight="bold">{element.name}</Typography>
          <Typography width={177} color="#787486">
            {descriptionParser(element.description)}
          </Typography>
          <PriorityStatusBox
            status={element.status}
            priority={element.priority}
          />
        </Box>
      </Box>
    </div>
  );
};

export default Todo;

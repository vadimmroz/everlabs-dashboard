import { MuiColorInput } from "mui-color-input";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setAddProject } from "@store/slices/addProjectSlice.ts";

const AddProjectColorPicker = () => {
  const dispatch = useAppDispatch();
  const addProject = useAppSelector((state) => state.addProject.addProject);
  const handleColorChange = (color: string) => {
    dispatch(setAddProject({ ...addProject, color }));
  };

  return (
    <MuiColorInput
      format="hex"
      value={addProject.color}
      onChange={handleColorChange}
    />
  );
};

export default AddProjectColorPicker;

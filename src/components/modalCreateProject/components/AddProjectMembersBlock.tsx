import AddMembers from "@components/addMembers";
import { Box } from "@mui/material";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setAddProject } from "@store/slices/addProjectSlice.ts";
import { membersRolesList } from "./utils.ts";

const AddProjectMembersBlock = () => {
  const addProject = useAppSelector((state) => state.addProject.addProject);
  const dispatch = useAppDispatch();

  const handleAddMembers = (
    name: string,
    type: "guest" | "moder" | "owner",
  ) => {
    dispatch(
      setAddProject({
        ...addProject,
        members: [...addProject.members, { email: name, role: type }],
      }),
    );
  };

  const handleDeleteMembers = (
    name: string,
    type: "guest" | "moder" | "owner",
  ) => {
    if (addProject) {
      dispatch(
        setAddProject({
          ...addProject,
          members: addProject.members.filter(
            (e) => e.email !== name && e.role !== type,
          ),
        }),
      );
    }
  };

  return (
    <Box display="flex" justifyContent="space-between">
      {membersRolesList.map((e: "guest" | "moder" | "owner", i) => {
        return (
          <AddMembers
            type={e}
            handleAddMembers={handleAddMembers}
            key={i}
            handleDeleteMembers={handleDeleteMembers}
          />
        );
      })}
    </Box>
  );
};

export default AddProjectMembersBlock;

import { TextField } from "@mui/material";
import { ChangeEvent } from "react";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setAddProject } from "@store/slices/addProjectSlice.ts";

const AddProjectNameField = () => {
  const addProject = useAppSelector((state) => state.addProject.addProject);
  const dispatch = useAppDispatch();
  const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(setAddProject({ ...addProject, name: event.target.value }));
  };
  return (
    <TextField
      label="Name*"
      onChange={handleNameChange}
      value={addProject.name}
    />
  );
};

export default AddProjectNameField;

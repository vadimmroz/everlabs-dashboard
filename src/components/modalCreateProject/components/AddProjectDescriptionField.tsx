import { TextField } from "@mui/material";
import { ChangeEvent } from "react";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setAddProject } from "@store/slices/addProjectSlice.ts";

const AddProjectDescriptionField = () => {
  const addProject = useAppSelector((state) => state.addProject.addProject);
  const dispatch = useAppDispatch();
  const handleDescriptionChange = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(setAddProject({ ...addProject, description: event.target.value }));
  };

  return (
    <TextField
      label="Description"
      multiline
      onChange={handleDescriptionChange}
      value={addProject.description}
    />
  );
};

export default AddProjectDescriptionField;

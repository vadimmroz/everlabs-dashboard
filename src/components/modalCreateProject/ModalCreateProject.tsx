import { Box, Button, Card, Typography } from "@mui/material";
import AddProjectMembersBlock from "./components/AddProjectMembersBlock.tsx";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import AddProjectNameField from "./components/AddProjectNameField.tsx";
import AddProjectDescriptionField from "./components/AddProjectDescriptionField.tsx";
import AddProjectColorPicker from "./components/AddProjectColorPicker.tsx";
import { setAddProject } from "@store/slices/addProjectSlice.ts";
import { Member } from "@/types/IProject.ts";
import { AddProject } from "@api/api.ts";
import { handleCloseProjectModal } from "@store/slices/modalAddProjectSlice.ts";

const ModalCreateProject = () => {
  const addProject = useAppSelector((state) => state.addProject.addProject);
  const dispatch = useAppDispatch();
  const handleClose = () => {
    dispatch(handleCloseProjectModal());
  };
  const handleProjectAdd = () => {
    handleClose();
    AddProject(addProject);
  };

  const handleDeleteMember = (member: Member) => {
    const temp = addProject.members.filter((e) => e.email !== member.email);
    dispatch(setAddProject({ ...addProject, members: temp }));
  };
  return (
    <div className="modal" onClick={handleClose}>
      <Card
        sx={{
          width: "100%",
          maxWidth: 1000,
          margin: "0 40px",
          display: "flex",
          flexDirection: "column",
          padding: 2,
          gap: 3,
        }}
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
        }}
      >
        <Typography sx={{ fontSize: 26, fontWeight: 600, margin: "10px auto" }}>
          Add Project
        </Typography>
        <AddProjectNameField />
        <AddProjectDescriptionField />
        <AddProjectColorPicker />
        <Typography fontWeight={600} fontSize={20}>
          Add Members:
        </Typography>

        <AddProjectMembersBlock />

        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            marginTop: 4,
            gap: 4,
          }}
        >
          {addProject?.members?.map((e, i) => {
            return (
              <Card sx={{ display: "flex", gap: 2, padding: 1 }} key={i}>
                <Box>
                  <Typography>
                    <span style={{ fontWeight: 600 }}>Role:</span> {e.role}
                  </Typography>
                  <Typography>
                    <span style={{ fontWeight: 600 }}>Email:</span> {e.email}
                  </Typography>
                </Box>
                {i > 0 && (
                  <Button
                    size="large"
                    color="error"
                    onClick={() => handleDeleteMember(e)}
                  >
                    &times;
                  </Button>
                )}
              </Card>
            );
          })}
        </Box>
        <Button
          color="secondary"
          variant="contained"
          disabled={!addProject.name}
          onClick={handleProjectAdd}
        >
          Add+
        </Button>
      </Card>
    </div>
  );
};

export default ModalCreateProject;

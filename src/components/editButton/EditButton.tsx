import styles from "./styles.ts";
import { Button } from "@mui/material";

const EditButton = ({
  editMode,
  handleEditMode,
}: {
  editMode: boolean;
  handleEditMode: () => void;
}) => {
  return (
    <Button sx={styles} variant="contained" onClick={handleEditMode}>
      {editMode ? "☑" : "✎"}
    </Button>
  );
};

export default EditButton;

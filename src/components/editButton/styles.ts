export default {
  position: "absolute",
  right: 20,
  bottom: 20,
  fontSize: 26,
  borderRadius: "50%",
};
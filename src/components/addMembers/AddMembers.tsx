import { ChangeEvent, useState } from "react";
import { Box, Button, TextField, Typography } from "@mui/material";
import { toTitleCase } from "@components/taskCard/utils.ts";

const AddMembers = ({
  type,
  list,
  handleAddMembers,
  handleDeleteMembers,
}: {
  type: "guest" | "moder" | "owner";
  list?: string[];
  handleAddMembers: (name: string, type: "guest" | "moder" | "owner") => void;
  handleDeleteMembers: (
    name: string,
    type: "guest" | "moder" | "owner",
  ) => void;
}) => {
  const [userName, setUserName] = useState("");

  const handleName = (e: ChangeEvent<HTMLInputElement>) => {
    setUserName(e.target.value);
  };

  const handleAdd = () => {
    handleAddMembers(userName, type);
    setUserName("");
  };
  return (
    <Box display="flex" flexDirection="column" gap={1} alignItems="center">
      <Typography>{toTitleCase(type)}</Typography>
      <TextField
        label={`Add ${toTitleCase(type)} +`}
        value={userName}
        onChange={handleName}
      />
      {userName && <Button onClick={handleAdd}>Add {type} +</Button>}
      {list?.map((e, i) => {
        return (
          <Box sx={{ display: "flex", gap: 1, alignItems: "center" }} key={i}>
            <Typography fontSize={12}>{e}</Typography>
            {localStorage.getItem("login") !== e && (
              <Button
                color="error"
                sx={{ fontSize: 20 }}
                onClick={() => handleDeleteMembers(e, type)}
              >
                &times;
              </Button>
            )}
          </Box>
        );
      })}
    </Box>
  );
};

export default AddMembers;

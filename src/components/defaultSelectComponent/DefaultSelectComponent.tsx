import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";

const DefaultSelectComponent = ({
  name,
  values,
  handler,
  value,
}: {
  name: string;
  values: string[];
  handler: (event: SelectChangeEvent<string>) => void;
  value: string | undefined;
}) => {
  return (
    <FormControl>
      <InputLabel id="demo-simple-select-standard-label">{name}</InputLabel>
      <Select
        labelId="demo-simple-select-standard-label"
        label="Priority"
        onChange={handler}
        defaultValue={value || ""}
      >
        {values?.map((e, i) => (
          <MenuItem value={e} key={i}>
            {e}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default DefaultSelectComponent;

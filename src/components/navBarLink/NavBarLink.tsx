import classes from "./navBarLink.module.css";
import { Typography } from "@mui/material";
import { NavLink } from "react-router-dom";

const NavBarLink = ({
  img,
  link,
  name,
}: {
  img: string;
  link: string;
  name: string;
}) => {
  return (
    <NavLink to={link} className={classes.navLink}>
      <img src={img} alt="" />
      <Typography>{name}</Typography>
    </NavLink>
  );
};

export default NavBarLink;

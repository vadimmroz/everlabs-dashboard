import { useEffect } from "react";
import { Button, Card, Box, Typography, FormControl } from "@mui/material";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { handleClose } from "@store/slices/modalAddTaskSlice.ts";
import { sampleInitialStateAddTask } from "./utils.ts";
import { useQuery, UseQueryResult } from "react-query";
import { getAllProjects, getProjectByName, postTask } from "@api/api.ts";
import { setProjectList } from "@store/slices/projectListSlice.ts";
import { setAddTask } from "@store/slices/addTaskSlice.ts";
import AddNameField from "./components/AddNameField.tsx";
import AddDescriptionField from "./components/AddDescriptionField.tsx";
import AddDeadline from "./components/AddDeadline.tsx";
import AddMembersBlock from "./components/AddMembersBlock.tsx";
import AddProjectChange from "./components/AddProjectChange.tsx";
import AddColorPicker from "./components/AddColorPicker.tsx";
import AddPriorityChanger from "./components/AddPriorityChanger.tsx";
import styles from "./styles.ts";
import { PostgrestSingleResponse } from "@supabase/supabase-js";
import { IProject } from "@/types/IProject.ts";

const ModalAddTask = () => {
  const dispatch = useAppDispatch();
  const addTask = useAppSelector((state) => state.addTask.addTask);
  const project: UseQueryResult<PostgrestSingleResponse<IProject[]>> = useQuery(
    ["project", addTask],
    () => getProjectByName(addTask.project),
  );
  const { data } = useQuery("projects", getAllProjects);

  useEffect(() => {
    if (data?.data) {
      dispatch(setProjectList(data?.data));
    }
  }, [data, dispatch]);

  const handleClickClose = () => {
    dispatch(handleClose());
  };

  const handleTaskAdd = () => {
    if (
      addTask.name &&
      addTask.priority &&
      addTask.project &&
      project?.data?.data
    ) {
      postTask(addTask, project?.data?.data[0]).then((response) => {
        if (response) {
          dispatch(
            setAddTask({
              ...sampleInitialStateAddTask,
              id: Math.floor(Math.random() * 1000),
            }),
          );
          dispatch(handleClose());
        }
      });
    }
  };

  return (
    <Box className="modal" onClick={handleClickClose}>
      <Card
        sx={styles.card}
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
        }}
      >
        <Button color="secondary" sx={styles.close} onClick={handleClickClose}>
          &times;
        </Button>
        <Typography mx="auto" my={1}>
          Add New Task
        </Typography>
        <FormControl sx={styles.form}>
          <AddNameField />
          <AddProjectChange />
          <AddDescriptionField />
          <AddDeadline />
          <AddColorPicker />
        </FormControl>
        <AddPriorityChanger />
        <Box display="flex" justifyContent="space-between" gap={1}>
          <AddMembersBlock />
        </Box>
        <Button
          color="secondary"
          variant="contained"
          disabled={!addTask.name || !addTask.priority || !addTask.project}
          onClick={handleTaskAdd}
        >
          Add+
        </Button>
      </Card>
    </Box>
  );
};

export default ModalAddTask;

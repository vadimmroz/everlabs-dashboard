export default {
  card: {
    maxWidth: 500,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    padding: 2,
    position: "relative",
    gap: 2,
  },
  close: {
    position: "absolute",
    right: 2,
    top: 2,
    fontSize: 20,
    borderRadius: "50%",
  },
  form: {
    display: "flex",
    justifyContent: "column",
    gap: 2,
  },
};

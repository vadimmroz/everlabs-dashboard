import { TextField } from "@mui/material";
import { ChangeEvent } from "react";
import { setAddTask } from "@store/slices/addTaskSlice.ts";
import { useAppDispatch, useAppSelector } from "@store/store.ts";

const AddDescriptionField = () => {
  const addTask = useAppSelector((state) => state.addTask.addTask);
  const dispatch = useAppDispatch();
  const handleDescriptionChange = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(setAddTask({ ...addTask, description: event.target.value }));
  };

  return (
    <TextField
      label="Description"
      multiline
      onChange={handleDescriptionChange}
      value={addTask.description}
    />
  );
};

export default AddDescriptionField;

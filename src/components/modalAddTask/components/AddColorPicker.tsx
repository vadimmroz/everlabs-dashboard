import { MuiColorInput } from "mui-color-input";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setAddTask } from "@store/slices/addTaskSlice.ts";

const AddColorPicker = () => {
  const dispatch = useAppDispatch();
  const addTask = useAppSelector((state) => state.addTask.addTask);
  const handleColorChange = (color: string) => {
    dispatch(setAddTask({ ...addTask, color }));
  };

  return (
    <MuiColorInput
      format="hex"
      value={addTask.color}
      onChange={handleColorChange}
    />
  );
};

export default AddColorPicker;

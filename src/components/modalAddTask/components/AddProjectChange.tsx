import DefaultSelectComponent from "@components/defaultSelectComponent";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { useEffect } from "react";
import { setAddTask } from "@store/slices/addTaskSlice.ts";
import { SelectChangeEvent } from "@mui/material";

const AddProjectChange = () => {
  const projectList = useAppSelector((state) => state.projectList.projectList);
  const dispatch = useAppDispatch();
  const addTask = useAppSelector((state) => state.addTask.addTask);
  const handleProjectChange = (event: SelectChangeEvent<string>) => {
    dispatch(setAddTask({ ...addTask, project: event.target.value }));
  };
  useEffect(() => {
    dispatch(
      setAddTask({
        ...addTask,
        projectColor: projectList?.filter(
          (e) => e?.name === addTask?.project,
        )?.[0]?.color,
        status: "To Do",
      }),
    );
  }, [addTask.project, dispatch, projectList]);
  return (
    <DefaultSelectComponent
      name="Project*"
      values={projectList?.map((e) => e.name)}
      handler={handleProjectChange}
      value=""
    />
  );
};

export default AddProjectChange;

import AddMembers from "@components/addMembers";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setAddTask } from "@store/slices/addTaskSlice.ts";
import { membersRolesListType } from "@/types/membersRolesListType.ts";
import { membersRolesList } from "./utils.ts";

const AddMembersBlock = () => {
  const addTask = useAppSelector((state) => state.addTask.addTask);
  const dispatch = useAppDispatch();

  const handleAddMembers = (
    name: string,
    type: "guest" | "moder" | "owner",
  ) => {
    dispatch(
      setAddTask({
        ...addTask,
        members: {
          ...addTask.members,
          [type]: [...addTask.members[type], name],
        },
      }),
    );
  };

  const handleDeleteMembers = (name: string, type: membersRolesListType) => {
    if (addTask) {
      dispatch(
        setAddTask({
          ...addTask,
          members: {
            ...addTask.members,
            [type]: addTask.members[type].filter((e) => e !== name),
          },
        }),
      );
    }
  };

  return (
    <>
      {membersRolesList.map((e: "guest" | "moder" | "owner", i) => {
        return (
          <AddMembers
            key={i}
            type={e}
            list={addTask.members[e]}
            handleAddMembers={handleAddMembers}
            handleDeleteMembers={handleDeleteMembers}
          />
        );
      })}
    </>
  );
};

export default AddMembersBlock;

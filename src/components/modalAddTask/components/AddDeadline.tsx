import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import { Box, Typography } from "@mui/material";
import {
  DatePicker,
  LocalizationProvider,
  TimePicker,
} from "@mui/x-date-pickers";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { Moment } from "moment";
import { setAddTask } from "@store/slices/addTaskSlice.ts";
import { refactorDate, refactorTime } from "@components/modalAddTask/utils.ts";

const AddDeadline = () => {
  const addTask = useAppSelector((state) => state.addTask.addTask);
  const dispatch = useAppDispatch();

  const handleDateChange = (event: Moment | null) => {
    if (event) {
      dispatch(
        setAddTask({
          ...addTask,
          deadline: refactorDate(event, addTask.deadline),
        }),
      );
    }
  };
  const handleTimeChange = (event: Moment | null) => {
    if (event) {
      dispatch(
        setAddTask({
          ...addTask,
          deadline: refactorTime(event, addTask.deadline),
        }),
      );
    }
  };

  return (
    <LocalizationProvider dateAdapter={AdapterMoment}>
      <Typography>Deadline</Typography>
      <Box sx={{ display: "flex", gap: 2 }}>
        <DatePicker onChange={handleDateChange} value={addTask.deadline} />
        <TimePicker onChange={handleTimeChange} value={addTask.deadline} />
      </Box>
    </LocalizationProvider>
  );
};

export default AddDeadline;

import { TextField } from "@mui/material";
import { ChangeEvent } from "react";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setAddTask } from "@store/slices/addTaskSlice.ts";

const AddNameField = () => {
  const addTask = useAppSelector((state) => state.addTask.addTask);
  const dispatch = useAppDispatch();
  const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(setAddTask({ ...addTask, name: event.target.value }));
  };
  return (
    <TextField label="Name*" onChange={handleNameChange} value={addTask.name} />
  );
};

export default AddNameField;

import DefaultSelectComponent from "@components/defaultSelectComponent";
import { SelectChangeEvent } from "@mui/material";
import { setAddTask } from "@store/slices/addTaskSlice.ts";
import { useAppDispatch, useAppSelector } from "@store/store.ts";

const AddPriorityChanger = () => {
  const dispatch = useAppDispatch();
  const addTask = useAppSelector((state) => state.addTask.addTask);
  const handlePriorityChange = (event: SelectChangeEvent<string>) => {
    dispatch(setAddTask({ ...addTask, priority: event.target.value }));
  };
  return (
    <DefaultSelectComponent
      name="Priority*"
      values={["low", "medium", "high"]}
      handler={handlePriorityChange}
      value="low"
    />
  );
};

export default AddPriorityChanger;

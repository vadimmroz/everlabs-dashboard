import moment, { Moment } from "moment/moment";

export const sampleInitialStateAddTask = {
  created_at: moment(),
  author: localStorage.getItem("login") || "",
  members: {
    guest: [],
    moder: [],
    owner: [localStorage.getItem("login") || ""],
  },
  deadline: moment(),
  comments: [],
  color: "#ffffff",
  id: Math.floor(Math.random() * 1000),
  name: "",
  project: "",
  priority: "",
  status: "",
  description: "",
  projectColor: "",
  estimate: 0,
};

export const refactorDate = (e: Moment | null, taskDeadline: Moment) => {
  return moment(
    moment(taskDeadline).format("hh:mm") + " " + moment(e).format("DD,MM,YYYY"),
  );
};
export const refactorTime = (e: Moment | null, taskDeadline: Moment) => {
  return moment(
    moment(e).format("hh:mm") + " " + moment(taskDeadline).format("DD,MM,YYYY"),
  );
};

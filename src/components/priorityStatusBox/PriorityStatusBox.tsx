import { Box, Typography } from "@mui/material";
import classes from "./priorityStasusBox.module.scss";

const PriorityStatusBox = ({
  status,
  priority,
}: {
  status: string;
  priority: string;
}) => {
  const statusClassName =
    classes.status +
    " " +
    (status === "In Progress"
      ? classes.inProgress
      : status === "Todo"
        ? classes.todo
        : classes.complete);
  const priorityClassName =
    classes.priority +
    " " +
    (priority === "low"
      ? classes.low
      : priority === "medium"
        ? classes.medium
        : classes.high);
  return (
    <Box display="flex" gap={1} maxWidth={170} mt={1}>
      <Box px={1} className={priorityClassName}>
        <Typography fontSize="14px">{priority}</Typography>
      </Box>
      <Box px={1} className={statusClassName}>
        <Typography fontSize="14px">{status}</Typography>
      </Box>
    </Box>
  );
};

export default PriorityStatusBox;

export const styleCard = {
  position: "relative",
  overflow: "unset",
  padding: 2,
  paddingTop: 3,
  display: "flex",
  flexDirection: "column",
  gap: 1,
  minHeight: 200,
  cursor: "pointer",
  justifyContent: "space-between",
};
export const styleButton = {
  borderRadius: "50%",
  position: "absolute",
  right: 20,
  top: 0,
  color: "black",
  fontSize: 20,
};

export const styleProjectName = {
  width: "30%",
  maxWidth: 100,
  height: 20,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  color: "white",
  position: "absolute",
  borderRadius: 1,
  top: -20,
  left: 20,
};

export const styleName = {
  color: "#0D062D",
  fontSize: 17,
  fontWeight: 600,
};

export const menu = {
  position: "absolute",
  display: "flex",
  background: "white",
  right: 20,
  top: 50,
  flexDirection: "column",
  boxShadow: "rgba(0, 0, 0, 0.16) 0px 1px 4px",
};

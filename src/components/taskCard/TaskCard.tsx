import { ITask } from "@/types/ITask.ts";
import { Box, Button, Card, Grid, Typography } from "@mui/material";
import { descriptionParser, membersRolesList, toTitleCase } from "./utils.ts";
import PriorityStatusBox from "@components/priorityStatusBox";
import commentsImg from "@src/assets/taskCardImg/message.svg";
import { useNavigate } from "react-router-dom";
import {
  styleButton,
  styleCard,
  styleName,
  styleProjectName,
} from "./styles.ts";
import { useEffect, useState, MouseEvent } from "react";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setSortedTaskList } from "@store/slices/sortedTaskListSlice.ts";
import MembersList from "@components/membersList";
import { IProfile } from "@/types/IProfile.ts";
import { deleteTask, getUsersProfilesByEmail } from "@api/api.ts";
import { PostgrestSingleResponse } from "@supabase/supabase-js";
import { Draggable } from "react-beautiful-dnd";

const TaskCard = ({ element }: { element: ITask }) => {
  const navigate = useNavigate();
  const [menuIsVisible, setMenuIsVisible] = useState(false);
  const dispatch = useAppDispatch();
  const sortedTaskList = useAppSelector(
    (state) => state.sortedTaskList.sortedTaskList,
  );
  const [users, setUsers] = useState<IProfile[]>([]);
  useEffect(() => {
    if (element) {
      setUsers([]);
      membersRolesList.forEach((key) => {
        getUsersProfilesByEmail(element.members[key]).then(
          (res: PostgrestSingleResponse<IProfile[]>) => {
            if (res.data) {
              res.data.forEach((e) => {
                if (!users.includes(e)) {
                  setUsers((prev) => [...prev, e]);
                }
              });
            }
          },
        );
      });
    }
  }, [element?.members, element]);

  const handleDelete = () => {
    setMenuIsVisible(false);
    deleteTask(element);
    dispatch(
      setSortedTaskList(sortedTaskList.filter((e) => e.id !== element.id)),
    );
  };

  const handleNavigateToProject = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    e.stopPropagation();
    navigate("/project" + element.project);
  };
  return (
    <Draggable draggableId={element.name} index={element.id}>
      {(provided) => (
        <Grid
          item
          py={4}
          gap={2}
          width="100%"
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <Card sx={styleCard} onClick={() => navigate("/task/" + element.id)}>
            <Button
              sx={styleButton}
              onClick={(e: MouseEvent<HTMLButtonElement>) => {
                e.preventDefault();
                e.stopPropagation();
                setMenuIsVisible(true);
              }}
            >
              ...
            </Button>
            {menuIsVisible && (
              <Box
                sx={{
                  position: "absolute",
                  display: "flex",
                  background: "white",
                  right: 20,
                  top: 50,
                  flexDirection: "column",
                  boxShadow: "rgba(0, 0, 0, 0.16) 0px 1px 4px",
                }}
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                }}
              >
                <Button onClick={handleDelete}>Delete</Button>
                <Button onClick={() => setMenuIsVisible(false)}>Cancel</Button>
              </Box>
            )}
            <Typography
              sx={{ ...styleProjectName, background: element.projectColor }}
              onClick={handleNavigateToProject}
            >
              {toTitleCase(element.project)}
            </Typography>
            <Typography sx={styleName}>{element.name}</Typography>
            <Typography
              sx={{
                color: "#787486",
                fontSize: 12,
              }}
            >
              {descriptionParser(element.description)}
            </Typography>
            <PriorityStatusBox
              status={element.status}
              priority={element.priority}
            />
            <Box display="flex" justifyContent="space-between">
              <Box width="30%">
                <MembersList members={users} />
              </Box>
              <Box
                display="flex"
                gap={1}
                justifyContent="center"
                alignItems="center"
              >
                <img src={commentsImg} alt="comentsImg" width={20} />
                <Typography color="#787486">
                  {element.comments.length} comments
                </Typography>
              </Box>
            </Box>
          </Card>
        </Grid>
      )}
    </Draggable>
  );
};

export default TaskCard;

import { membersRolesListType } from "@/types/membersRolesListType.ts";

export const toTitleCase = (str: string | undefined) => {
  if (str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
  return;
};
export const descriptionParser = (str: string) => {
  if (str.length > 240) {
    return str.slice(0, -(str.length - 240)).trim() + "...";
  } else {
    return str;
  }
};

export const membersRolesList: membersRolesListType[] = [
  "guest",
  "moder",
  "owner",
];

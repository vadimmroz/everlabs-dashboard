import { Box, Button, FormGroup, TextField, Typography } from "@mui/material";
import React, { useState } from "react";
import { signUpNewUser } from "@api/api.ts";
import { useNavigate } from "react-router-dom";

const RegisterPage = () => {
  const [userData, setUserData] = useState({
    email: "",
    password: "",
    phone: "",
  });
  const [error, setError] = useState({
    isError: false,
    message: "",
  });

  const navigate = useNavigate();

  const handleChangeInput = (
    e: React.ChangeEvent<HTMLInputElement>,
    name: string,
  ) => {
    setError({
      isError: false,
      message: "",
    });
    setUserData((prevState) => ({ ...prevState, [name]: e.target.value }));
  };

  const handleSubmit = () => {
    if (Number(userData.phone).toString().length > 9) {
      signUpNewUser(userData.email, userData.password, userData.phone).then(
        (res) => {
          if (res?.data.user) {
            localStorage.setItem("loginPage", userData.email);
            localStorage.setItem("password", userData.password);
            navigate("/tasks");
          } else {
            setError({
              isError: true,
              message: res.error?.message || "something went wrong",
            });
          }
        },
      );
    } else {
      setError({
        isError: true,
        message: "Phone number is not correct",
      });
    }
  };

  return (
    <Box>
      <FormGroup sx={{ minWidth: 500, gap: 2, padding: 2 }}>
        <Typography>Registered Form</Typography>
        <TextField
          label="Email"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            handleChangeInput(e, "email")
          }
          required
          variant="outlined"
          color="secondary"
          type="email"
          sx={{ mb: 3 }}
          fullWidth
          error={error.isError}
          helperText={error.message.includes("email") ? error.message : ""}
          value={userData.email}
        />
        <TextField
          label="Password"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            handleChangeInput(e, "password")
          }
          required
          variant="outlined"
          color="secondary"
          type="password"
          error={error.isError}
          value={userData.password}
          fullWidth
          helperText={error.message.includes("Password") ? error.message : ""}
          sx={{ mb: 3 }}
        />
        <TextField
          label="Phone Number"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            handleChangeInput(e, "phone")
          }
          required
          variant="outlined"
          color="secondary"
          type="tel"
          value={userData.phone}
          helperText={error.message.includes("Phone") ? error.message : ""}
          error={error.isError}
          fullWidth
          sx={{ mb: 3 }}
        />
        <Button
          variant="outlined"
          color="secondary"
          type="submit"
          onClick={handleSubmit}
        >
          Registered
        </Button>
      </FormGroup>
    </Box>
  );
};

export default RegisterPage;

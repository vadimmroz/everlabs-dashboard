import { Box, Button, FormGroup, TextField, Typography } from "@mui/material";
import React, { useState } from "react";
import { signInWithEmail } from "@api/api.ts";
import { useNavigate } from "react-router-dom";

const LoginPage = () => {
  const [userData, setUserData] = useState({
    email: "",
    password: "",
  });
  const [error, setError] = useState(false);
  const navigate = useNavigate();

  const handleChangeInput = (
    e: React.ChangeEvent<HTMLInputElement>,
    name: string,
  ) => {
    setError(false);
    setUserData((prevState) => ({ ...prevState, [name]: e.target.value }));
  };

  const handleSubmit = async () => {
    try {
      const res = await signInWithEmail(userData.email, userData.password);
      if (res?.data.user) {
        localStorage.setItem("login", userData.email);
        localStorage.setItem("password", userData.password);
        navigate("/home/tasks");
      } else {
        setUserData((prevState) => ({ ...prevState, password: "" }));
        setError(true);
      }
    } catch (error) {
      setError(true);
      setUserData((prevState) => ({ ...prevState, password: "" }));
      console.error("Помилка під час обробки логіну:", error);
    }
  };

  return (
    <Box>
      <FormGroup sx={{ minWidth: 500, gap: 2, padding: 2 }}>
        <Typography>Login Form</Typography>
        <TextField
          label="Email"
          name="email"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            handleChangeInput(e, "email")
          }
          required
          variant="outlined"
          color="secondary"
          type="email"
          error={error}
          sx={{ mb: 3 }}
          fullWidth
          value={userData.email}
          helperText={error && "Email or password is incorrect"}
        />
        <TextField
          label="Password"
          name="password"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            handleChangeInput(e, "password")
          }
          required
          error={error}
          variant="outlined"
          color="secondary"
          type="password"
          value={userData.password}
          fullWidth
          sx={{ mb: 3 }}
          helperText={error && "Email or password is incorrect"}
        />
        <Button
          variant="outlined"
          color="secondary"
          type="submit"
          onClick={handleSubmit}
        >
          Login
        </Button>
      </FormGroup>
    </Box>
  );
};

export default LoginPage;

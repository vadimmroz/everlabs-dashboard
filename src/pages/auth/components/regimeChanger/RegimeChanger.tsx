import { Button, Typography } from "@mui/material";

const RegimeChanger = ({
  text,
  title,
  setTab,
}: {
  text: string;
  title: string;
  setTab: (regime: string) => void;
}) => {
  return (
    <Typography>
      {title} <Button onClick={() => setTab(text)}>{text}</Button>
    </Typography>
  );
};

export default RegimeChanger;

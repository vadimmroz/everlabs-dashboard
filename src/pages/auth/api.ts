import { signInWithEmail, signUpNewUser } from "@api/api.ts";

export const ligIn = ({
  email,
  password,
}: {
  email: string;
  password: string;
}) => {
  return signInWithEmail(email, password);
};

export const register = ({
  email,
  password,
  phone,
}: {
  email: string;
  password: string;
  phone: string;
}) => {
  return signUpNewUser(email, password, phone);
};

import { Box } from "@mui/material";
import { lazy, Suspense, useState } from "react";
import RegimeChanger from "./components/regimeChanger";

const LoginPage = lazy(() => import("./pages/loginPage"));
const RegisterPage = lazy(() => import("./pages/registerPage"));

const AuthPage = () => {
  const [tab, setTab] = useState("login");

  return (
    <Box
      sx={{ display: "flex", alignItems: "center" }}
      mt={20}
      flexDirection="column"
    >
      {tab === "login" ? (
        <>
          <Suspense>
            <LoginPage />
          </Suspense>
          <RegimeChanger
            text="registered"
            title="Need an account?"
            setTab={setTab}
          />
        </>
      ) : (
        <>
          <Suspense>
            <RegisterPage />
          </Suspense>
          <RegimeChanger
            text="login"
            title="Already have account?"
            setTab={setTab}
          />
        </>
      )}
    </Box>
  );
};

export default AuthPage;

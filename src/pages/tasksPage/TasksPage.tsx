import { Grid } from "@mui/material";
import TaskList from "@components/taskList";
import { taskLists } from "./utils.ts";
import { DragDropContext, DropResult } from "react-beautiful-dnd";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setSortedTaskList } from "@store/slices/sortedTaskListSlice.ts";
import { updateTask } from "@api/api.ts";

const TasksPage = () => {
  const sortedTaskList = useAppSelector(
    (state) => state.sortedTaskList.sortedTaskList,
  );
  const dispatch = useAppDispatch();
  const handleOnDragEnd = (result: DropResult) => {
    if (result.destination) {
      const newPositionTasksInList = sortedTaskList.map((task) => {
        if (task.id === result.source.index) {
          task = { ...task, status: result.destination?.droppableId || "" };
          updateTask(task);
          return task;
        }
        return task;
      });
      dispatch(setSortedTaskList(newPositionTasksInList));
    }
  };
  return (
    <>
      <Grid item xs={3} sx={{ overflowX: "scroll", width: 1440 }}>
        <Grid container columns={{ xs: 3.2 }} gap={5} pl={2}>
          <DragDropContext onDragEnd={handleOnDragEnd}>
            {taskLists.map((e, i) => (
              <TaskList key={i} name={e.name} color={e.color} />
            ))}
          </DragDropContext>
        </Grid>
      </Grid>
    </>
  );
};

export default TasksPage;

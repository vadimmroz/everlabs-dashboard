export const taskLists = [
  { name: "To Do", color: "#973FCF" },
  { name: "In Progress", color: "#FFA500" },
  { name: "Complete", color: "#8BC48A" },
];

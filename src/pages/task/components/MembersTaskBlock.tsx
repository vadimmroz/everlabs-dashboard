import { Box, Typography } from "@mui/material";
import MembersList from "@components/membersList";
import AddMembers from "@components/addMembers/AddMembers.tsx";
import { useEffect, useState } from "react";
import { IProfile } from "@/types/IProfile.ts";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setTask } from "@store/slices/taskSlice.ts";
import { getUsersProfilesByEmail } from "@api/api.ts";
import { PostgrestSingleResponse } from "@supabase/supabase-js";
import { membersRolesListType } from "@/types/membersRolesListType.ts";
import { membersRolesList } from "@components/taskCard/utils.ts";

const MembersTaskBlock = ({ editMode }: { editMode: boolean }) => {
  const task = useAppSelector((state) => state.task.task);
  const [users, setUsers] = useState<IProfile[]>([]);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (task) {
      setUsers([]);
      membersRolesList.forEach((key) => {
        getUsersProfilesByEmail(task.members[key]).then(
          (res: PostgrestSingleResponse<IProfile[]>) => {
            if (res.data) {
              res.data.forEach((e) => {
                setUsers((prev) => [...prev, e]);
              });
            }
          },
        );
      });
    }
  }, [task?.members, task]);

  const handleAddMembers = (
    name: string,
    type: "guest" | "moder" | "owner",
  ) => {
    if (task) {
      dispatch(
        setTask({
          ...task,
          members: { ...task.members, [type]: [...task.members[type], name] },
        }),
      );
    }
  };
  const handleDeleteMembers = (name: string, type: membersRolesListType) => {
    if (task) {
      dispatch(
        setTask({
          ...task,
          members: {
            ...task.members,
            [type]: task.members[type].filter((e) => e !== name),
          },
        }),
      );
    }
  };

  return (
    <Box display="flex" gap={4}>
      <Box>
        <Typography fontWeight={600}>Members:</Typography>
        {users && <MembersList members={users} />}
        <Box display="flex" gap={2}>
          {editMode &&
            task &&
            membersRolesList?.map((e) => {
              return (
                <AddMembers
                  type={e}
                  list={task.members[e]}
                  handleAddMembers={handleAddMembers}
                  handleDeleteMembers={handleDeleteMembers}
                />
              );
            })}
        </Box>
      </Box>
    </Box>
  );
};

export default MembersTaskBlock;

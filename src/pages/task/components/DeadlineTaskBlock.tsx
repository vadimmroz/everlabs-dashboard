import { Box, Typography } from "@mui/material";
import moment from "moment";
import { useAppSelector } from "@store/store.ts";

const DeadlineTaskBlock = () => {
  const task = useAppSelector((state) => state.task.task);
  return (
    <Box>
      <Typography fontWeight={600}>
        Deadline: {moment(task?.deadline).format("hh:mm DD,MM,YYYY")}
      </Typography>
    </Box>
  );
};

export default DeadlineTaskBlock;

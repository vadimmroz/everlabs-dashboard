import { Box, TextField, Typography } from "@mui/material";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setTask } from "@store/slices/taskSlice.ts";
import { ChangeEvent } from "react";

const DescriptionTaskBlock = ({ editMode }: { editMode: boolean }) => {
  const task = useAppSelector((state) => state.task.task);

  const dispatch = useAppDispatch();

  const handleInput = (e: ChangeEvent<HTMLInputElement>) => {
    if (task) {
      dispatch(setTask({ ...task, description: e.target.value }));
    }
  };

  return (
    <Box>
      {!editMode ? (
        <Typography>{task?.description}</Typography>
      ) : (
        <TextField
          fullWidth
          value={task?.description}
          label="description"
          multiline
          onChange={handleInput}
        />
      )}
    </Box>
  );
};

export default DescriptionTaskBlock;

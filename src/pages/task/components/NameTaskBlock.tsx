import { Box, TextField, Typography } from "@mui/material";
import { toTitleCase } from "@components/taskCard/utils.ts";
import { setTask } from "@store/slices/taskSlice.ts";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { ChangeEvent } from "react";

const NameTaskBlock = ({ editMode }: { editMode: boolean }) => {
  const task = useAppSelector((state) => state.task.task);
  const dispatch = useAppDispatch();
  const handleInput = (e: ChangeEvent<HTMLInputElement>) => {
    if (task) {
      dispatch(setTask({ ...task, name: e.target.value }));
    }
  };
  return (
    <Box>
      {!editMode ? (
        <Typography fontSize={20} fontWeight={600}>
          {toTitleCase(task?.name)}
        </Typography>
      ) : (
        <TextField
          label="name"
          value={task?.name}
          fullWidth
          onChange={handleInput}
        />
      )}
      <Typography color="#787486" fontSize={12}>
        from "{task?.status}" column
      </Typography>
    </Box>
  );
};

export default NameTaskBlock;

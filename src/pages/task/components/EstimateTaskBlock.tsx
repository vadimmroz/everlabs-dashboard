import { Box, TextField, Typography } from "@mui/material";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { ChangeEvent } from "react";
import { setTask } from "@store/slices/taskSlice.ts";

const EstimateTaskBlock = ({ editMode }: { editMode: boolean }) => {
  const task = useAppSelector((state) => state.task.task);
  const dispatch = useAppDispatch();
  const handleInput = (e: ChangeEvent<HTMLInputElement>) => {
    if (task) {
      dispatch(setTask({ ...task, estimate: Number(e.target.value) }));
    }
  };
  return (
    <Box p={2} display="flex" flexDirection="column" gap={1}>
      <Typography fontWeight={600}>Estimate:</Typography>
      {!editMode ? (
        <Typography fontSize="14px">{task?.estimate}</Typography>
      ) : (
        <TextField value={task?.estimate} onChange={handleInput} />
      )}
    </Box>
  );
};

export default EstimateTaskBlock;

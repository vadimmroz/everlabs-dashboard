import { Box, SelectChangeEvent, Typography } from "@mui/material";
import classes from "@components/myTodoList/myTodoList.module.css";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setTask } from "@store/slices/taskSlice.ts";
import DefaultSelectComponent from "@components/defaultSelectComponent";

const PriorityBlock = ({
  status,
  editMode,
}: {
  status: string | undefined;
  editMode: boolean;
}) => {
  const task = useAppSelector((state) => state.task.task);
  const dispatch = useAppDispatch();

  const statusClassName =
    classes.status +
    " " +
    (status === "In Progress"
      ? classes.inProgress
      : status === "Todo"
        ? classes.todo
        : classes.complete);

  const handleChange = (e: SelectChangeEvent<string>) => {
    if (task) {
      dispatch(setTask({ ...task, status: e.target.value }));
    }
  };
  return (
    <Box p={2} display="flex" flexDirection="column" gap={1}>
      <Typography fontWeight={600}>Status:</Typography>
      <div className={statusClassName}>
        {!editMode ? (
          <Typography fontSize="14px">{status}</Typography>
        ) : (
          <DefaultSelectComponent
            name="status"
            values={["To Do", "In Progress", "Complete"]}
            handler={handleChange}
            value={status}
          />
        )}
      </div>
    </Box>
  );
};

export default PriorityBlock;

import { Box, SelectChangeEvent, Typography } from "@mui/material";
import classes from "@components/myTodoList/myTodoList.module.css";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setTask } from "@store/slices/taskSlice.ts";
import DefaultSelectComponent from "@components/defaultSelectComponent";

const PriorityBlock = ({
  priority,
  editMode,
}: {
  priority: string | undefined;
  editMode: boolean;
}) => {
  const task = useAppSelector((state) => state.task.task);
  const dispatch = useAppDispatch();

  const priorityClassName =
    classes.priority +
    " " +
    (priority === "low"
      ? classes.low
      : priority === "medium"
        ? classes.medium
        : classes.high);

  const handleChange = (e: SelectChangeEvent<string>) => {
    if (task) {
      dispatch(setTask({ ...task, priority: e.target.value }));
    }
  };
  return (
    <Box p={2} display="flex" flexDirection="column" gap={1}>
      <Typography fontWeight={600}>Priority:</Typography>
      <div className={priorityClassName}>
        {!editMode ? (
          <Typography fontSize="14px">{priority}</Typography>
        ) : (
          <DefaultSelectComponent
            name="priority"
            values={["low", "medium", "high"]}
            handler={handleChange}
            value={priority}
          />
        )}
      </div>
    </Box>
  );
};

export default PriorityBlock;

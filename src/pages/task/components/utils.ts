import { membersRolesListType } from "@/types/membersRolesListType.ts";

export const membersRolesList: membersRolesListType[] = [
  "guest",
  "moder",
  "owner",
];

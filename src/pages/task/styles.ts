export default {
  container: {
    display: "flex",
    padding: 2,
    flexDirection: "column",
    maxWidth: 700,
    margin: "0 auto",
    minHeight: "80vh",
    gap: 4,
  },
};

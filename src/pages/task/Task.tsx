import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import { getTaskById, updateTask } from "@api/api.ts";
import { Box, Card } from "@mui/material";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { useEffect, useState } from "react";
import { setTask } from "@store/slices/taskSlice.ts";
import styles from "./styles.ts";
import { useCheckRoleUser } from "./hooks.ts";
import PriorityBlock from "./components/PriorityBlock.tsx";
import StatusBlock from "./components/StatusBlock.tsx";
import NameTaskBlock from "./components/NameTaskBlock.tsx";
import DescriptionTaskBlock from "./components/DescriptionTaskBlock.tsx";
import DeadlineTaskBlock from "./components/DeadlineTaskBlock.tsx";
import EstimateTaskBlock from "./components/EstimateTaskBlock.tsx";
import MembersTaskBlock from "./components/MembersTaskBlock.tsx";
import EditButton from "@components/editButton";

const Task = () => {
  const params = useParams();
  const dispatch = useAppDispatch();
  const canEdit = useCheckRoleUser();
  const task = useAppSelector((state) => state.task.task);
  const [editMode, setEditMode] = useState(false);
  const { data } = useQuery("task", () => getTaskById(params), {
    refetchOnWindowFocus: false,
  });

  useEffect(() => {
    if (data?.data) {
      dispatch(setTask(data.data[0]));
    }
  }, [data, dispatch]);

  const handleSave = () => {
    if (task) {
      updateTask(task);
    }
  };

  const handleEditMode = () => {
    setEditMode((prev) => {
      if (prev) {
        handleSave();
      }
      return !prev;
    });
  };

  return (
    <Card sx={styles.container}>
      {task && (
        <>
          {canEdit && (
            <EditButton editMode={editMode} handleEditMode={handleEditMode} />
          )}
          <NameTaskBlock editMode={editMode} />
          <MembersTaskBlock editMode={editMode} />
          <DeadlineTaskBlock />
          <DescriptionTaskBlock editMode={editMode} />
          <Box display="flex" gap={2}>
            <PriorityBlock priority={task?.priority} editMode={editMode} />
            <StatusBlock status={task?.status} editMode={editMode} />
            <EstimateTaskBlock editMode={editMode} />
          </Box>
        </>
      )}
    </Card>
  );
};

export default Task;

import { useAppSelector } from "@store/store.ts";

export const useCheckRoleUser = () => {
  const task = useAppSelector((state) => state.task.task);
  const user = localStorage.getItem("login") || "";
  if (task?.members.guest.includes(user)) {
    return false;
  } else if (
    task?.members.moder.includes(user) ||
    task?.members.owner.includes(user)
  ) {
    return true;
  }
  return false;
};

import { Card, Grid, Typography } from "@mui/material";
import moment from "moment/moment";
import { Link } from "react-router-dom";
import { IProject } from "@/types/IProject.ts";

const ProjectItem = ({ element }: { element: IProject }) => {
  return (
    <Grid item xs={1}>
      <Link to={"/project/" + element.name}>
        <Card sx={{ background: element.color, padding: 3, borderRadius: 2 }}>
          <Typography color="#fff" fontWeight="bolder">
            {element.name}
          </Typography>
          <Typography color="#e5e5e5">{element.description}</Typography>
          <Typography color="#fff">
            created at
            {" " + moment(element.created_at).format("DD MMMM YYYY HH:mm")}
          </Typography>
          <Typography color="#fff">author {element.author}</Typography>
        </Card>
      </Link>
    </Grid>
  );
};

export default ProjectItem;

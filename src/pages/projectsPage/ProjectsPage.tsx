import { useQuery } from "react-query";
import { getAllProjects } from "@api/api.ts";
import { useEffect } from "react";
import ProjectItem from "./components/projectItem";
import { useAppSelector, useAppDispatch } from "@store/store.ts";
import { setProjectList } from "@store/slices/projectListSlice.ts";
import { Grid } from "@mui/material";
import { IProject } from "@/types/IProject.ts";

const ProjectsPage = () => {
  const { data } = useQuery("projects", getAllProjects);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (data?.data) {
      dispatch(setProjectList(data?.data));
    }
  }, [data, dispatch]);

  const projectList = useAppSelector((state) => state.projectList.projectList);

  return (
    <Grid item mt={10} xs={3}>
      <Grid
        pl={5}
        pr={2}
        pt={1}
        container
        columnSpacing={2}
        rowGap={2}
        columns={{ xs: 3 }}
      >
        {projectList &&
          projectList.map((e: IProject) => (
            <ProjectItem element={e} key={e.id} />
          ))}
      </Grid>
    </Grid>
  );
};

export default ProjectsPage;

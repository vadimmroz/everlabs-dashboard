import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { TextField, Typography } from "@mui/material";
import { setProject } from "@store/slices/projectSlice.ts";
import { ChangeEvent } from "react";
import { toTitleCase } from "@components/taskCard/utils.ts";

const ProjectControlName = ({ editMode }: { editMode: boolean }) => {
  const project = useAppSelector((state) => state.project.project);
  const dispatch = useAppDispatch();

  const handleName = (e: ChangeEvent<HTMLInputElement>) => {
    if (project) {
      dispatch(setProject({ ...project, name: e.target.value }));
    }
  };
  return (
    <>
      {!editMode ? (
        <Typography fontWeight={800} fontSize={35}>
          {toTitleCase(project?.name)}
        </Typography>
      ) : (
        <TextField
          label="name"
          multiline
          value={project?.name}
          onChange={handleName}
        />
      )}
    </>
  );
};

export default ProjectControlName;

import { useAppDispatch, useAppSelector } from "@store/store.ts";
import AddMembers from "@components/addMembers";
import { setProject } from "@store/slices/projectSlice.ts";
import { membersRolesListType } from "@/types/membersRolesListType.ts";
import { membersRolesList } from "./utils.ts";

const ProjectControlAddMember = () => {
  const project = useAppSelector((state) => state.project.project);
  const dispatch = useAppDispatch();

  const handleAddMembers = (name: string, type: membersRolesListType) => {
    if (project) {
      dispatch(
        setProject({
          ...project,
          members: [...project.members, { role: type, email: name }],
        }),
      );
    }
  };

  const handleDeleteMembers = (name: string, type: membersRolesListType) => {
    if (project) {
      dispatch(
        setProject({
          ...project,
          members: project.members.filter(
            (e) => e.email !== name && e.role !== type,
          ),
        }),
      );
    }
  };

  return (
    <>
      {membersRolesList.map((e: membersRolesListType) => {
        return (
          <AddMembers
            type={e}
            list={project?.members
              .filter((member) => member.role === e)
              .map((member) => member.email)}
            handleAddMembers={handleAddMembers}
            handleDeleteMembers={handleDeleteMembers}
          />
        );
      })}
    </>
  );
};

export default ProjectControlAddMember;

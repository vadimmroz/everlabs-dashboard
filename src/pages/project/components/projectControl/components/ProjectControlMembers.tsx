import { useEffect, useState } from "react";
import { useAppSelector } from "@store/store.ts";
import { getUsersProfilesByEmail } from "@api/api.ts";
import { PostgrestSingleResponse } from "@supabase/supabase-js";
import { IProfile } from "@/types/IProfile.ts";
import MembersList from "@components/membersList";
import { Box } from "@mui/material";
import ProjectControlAddMember from "@/pages/project/components/projectControl/components/ProjectControlAddMember.tsx";

const ProjectControlMembers = ({ editMode }: { editMode: boolean }) => {
  const [users, setUsers] = useState<IProfile[]>([]);
  const project = useAppSelector((state) => state.project.project);

  useEffect(() => {
    if (project?.members) {
      getUsersProfilesByEmail(project?.members.map((e) => e.email)).then(
        (e: PostgrestSingleResponse<IProfile[]>) => {
          if (e.data) {
            setUsers(e.data);
          }
        },
      );
    }
  }, [project?.members]);

  return (
    <>
      {users && <MembersList members={users} />}
      {editMode && (
        <Box display="flex" flexDirection="column" alignItems="flex-start">
          <ProjectControlAddMember />
        </Box>
      )}
    </>
  );
};

export default ProjectControlMembers;

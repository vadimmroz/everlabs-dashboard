import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { ChangeEvent } from "react";
import { setProject } from "@store/slices/projectSlice.ts";
import { TextField, Typography } from "@mui/material";

const ProjectControlDescription = ({ editMode }: { editMode: boolean }) => {
  const project = useAppSelector((state) => state.project.project);
  const dispatch = useAppDispatch();

  const handleDescription = (e: ChangeEvent<HTMLInputElement>) => {
    if (project) {
      dispatch(setProject({ ...project, description: e.target.value }));
    }
  };
  return (
    <>
      {!editMode ? (
        <Typography color="#787486">{project?.description}</Typography>
      ) : (
        <TextField
          label="Description"
          value={project?.description}
          multiline
          onChange={handleDescription}
        />
      )}
    </>
  );
};

export default ProjectControlDescription;

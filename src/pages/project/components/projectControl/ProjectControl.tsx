import { Box, Grid } from "@mui/material";
import { useAppSelector } from "@store/store";
import { useState } from "react";
import EditButton from "@components/editButton";
import ProjectControlName from "./components/ProjectControlName.tsx";
import { saveProject } from "@api/api.ts";
import ProjectControlDescription from "@/pages/project/components/projectControl/components/ProjectControlDescription.tsx";
import ProjectControlMembers from "@/pages/project/components/projectControl/components/ProjectControlMembers.tsx";

const ProjectControl = () => {
  const project = useAppSelector((state) => state.project.project);
  const [editMode, setEditMode] = useState(false);
  const handleSaveProject = () => {
    if (project) {
      saveProject(project);
    }
  };

  const handleEditMode = () => {
    setEditMode((prev) => {
      if (prev) {
        handleSaveProject();
      }
      return !prev;
    });
  };

  return (
    <Grid item xs={1}>
      <Box sx={{ display: "flex", flexDirection: "column", gap: 1 }}>
        <ProjectControlName editMode={editMode} />
        <ProjectControlDescription editMode={editMode} />
        <ProjectControlMembers editMode={editMode} />
      </Box>
      <EditButton editMode={editMode} handleEditMode={handleEditMode} />
    </Grid>
  );
};

export default ProjectControl;

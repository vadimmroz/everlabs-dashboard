import { useParams } from "react-router-dom";
import { useQuery, UseQueryResult } from "react-query";
import {
  getAllTaskByProjectName,
  getProjectById,
  updateTask,
} from "@api/api.ts";
import { PostgrestResponse } from "@supabase/supabase-js";
import { Grid } from "@mui/material";
import TaskList from "@components/taskList";
import { taskLists } from "./utils.ts";
import { IProject } from "@/types/IProject.ts";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { useEffect } from "react";
import { setProject } from "@store/slices/projectSlice.ts";
import ProjectControl from "./components/projectControl";
import { setSortedTaskList } from "@store/slices/sortedTaskListSlice.ts";
import { ITask } from "@/types/ITask.ts";
import { DragDropContext, DropResult } from "react-beautiful-dnd";

const Project = () => {
  const params = useParams();
  const sortedTaskList = useAppSelector(
    (state) => state.sortedTaskList.sortedTaskList,
  );
  const { data }: { data: PostgrestResponse<IProject> | undefined } = useQuery(
    "project",
    () => getProjectById(params),
  );
  const tasks: UseQueryResult<PostgrestResponse<ITask>> = useQuery(
    ["tasks", data?.data],
    () => getAllTaskByProjectName(params),
  );

  const dispatch = useAppDispatch();

  useEffect(() => {
    if (data?.data) {
      dispatch(setProject(data?.data[0]));
    }
  }, [data, dispatch]);

  useEffect(() => {
    if (tasks?.data?.data) {
      dispatch(setSortedTaskList(tasks.data.data));
    }
  }, [tasks?.data?.data, dispatch]);

  const handleOnDragEnd = (result: DropResult) => {
    if (result?.destination?.droppableId) {
      const newPositionTasksInList = sortedTaskList.map((task) => {
        if (task.id === result.source.index) {
          task = { ...task, status: result.destination?.droppableId || "" };
          updateTask(task);
          return task;
        }
        return task;
      });
      dispatch(setSortedTaskList(newPositionTasksInList));
    }
  };

  return (
    <>
      {data?.data && (
        <Grid container columnSpacing={2} rowGap={2} columns={{ xs: 4 }} p={2}>
          <ProjectControl />
          <Grid item xs={3}>
            <Grid container columns={{ xs: 3 }}>
              <DragDropContext onDragEnd={handleOnDragEnd}>
                {taskLists.map((e, i) => (
                  <TaskList key={i} name={e.name} color={e.color} />
                ))}
              </DragDropContext>
            </Grid>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default Project;

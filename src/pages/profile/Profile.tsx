import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { getUserProfileData } from "@api/api.ts";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "@store/store.ts";
import { setProfile } from "@store/slices/profileSlice.ts";

const Profile = () => {
  const params = useParams();
  const dispatch = useAppDispatch();
  const { data } = useQuery(["profile", params], () =>
    getUserProfileData(params?.id),
  );
  const profile = useAppSelector((state) => state.profile.profile);
  useEffect(() => {
    if (data?.data) {
      dispatch(setProfile(data?.data[0]));
    }
  }, [data]);

  useEffect(() => {
    console.log(profile);
  }, [profile]);

  return <div></div>;
};

export default Profile;
